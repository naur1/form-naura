document.addEventListener("DOMContentLoaded", function() {
    var form = document.getElementById("form-data-diri");
    var submitButton = document.getElementById("submit");

    submitButton.addEventListener("click", function() {
        var nama = document.getElementById("nama").value;
        var jenisKelamin = document.querySelector('input[name="jenisKelamin"]:checked').value;
        var jurusan = document.getElementById("jurusan").value;
        alert("Nama : " + nama + "\nJenis Kelamin : " + jenisKelamin + "\nJurusan : " + jurusan);
    });
});